<<<<<<< HEAD
package com.aftery.controller;

import cn.hutool.log.StaticLog;
import com.aftery.bean.returnJson;
import com.sun.corba.se.spi.orb.ORB;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.rmi.MarshalledObject;
import java.util.HashMap;
import java.util.Map;

@Controller
public class UserController {

    @ResponseBody
    @RequestMapping("/user/{id}")
    public returnJson user(@PathVariable("id") Integer id, HttpServletRequest request){
        returnJson json=new returnJson();
        json.setId(id);
        json.setPort(request.getLocalPort()+"{2.0-2.0}");
        json.setSessionId(request.getSession().getId());
       return json;
    }

    @ResponseBody
    @GetMapping("/testsesiion")
    public Object test12(HttpServletRequest request){
        HttpSession session = request.getSession();
        String name = session.getClass().getName();
        String id = session.getId();
        StaticLog.info("sessionName-->>"+name);
        StaticLog.info("sessionid---->>"+id);
        Map<String,Object> map=new HashMap<>();
        map.put("sessionName",name);
        map.put("sessionId",id);
        session.setAttribute("key",map);
        return  map;
    }
}
=======
package com.aftery.controller;

import cn.hutool.log.StaticLog;
import com.aftery.bean.User;
import com.aftery.bean.returnJson;
import com.alibaba.fastjson.parser.ParserConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@RestController
public class UserController {


    @RequestMapping("/user/{id}")
    public returnJson user(@PathVariable("id") Integer id, HttpServletRequest request) {
        returnJson json = new returnJson();
        json.setId(id);
        json.setPort(request.getLocalPort() + "{2.0-2.0}");
        json.setSessionId(request.getSession().getId());
        return json;
    }


    @GetMapping("/testsesiion")
    public Object test12(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String name = session.getClass().getName();
        String id = session.getId();
        StaticLog.info("sessionName-->>" + name);
        StaticLog.info("sessionid---->>" + id);
        Map<String, Object> map = new HashMap<>();
        map.put("sessionName", name);
        map.put("sessionId", id);
        session.setAttribute("key", map);
        return map;
    }

    @RequestMapping("/getUser")
    @Cacheable("user-key")
    public User getUser() {
        ParserConfig.getGlobalInstance().setAutoTypeSupport(true);
        User user = new User(1L, "aa", "123456", "123456@qq.com", "aa", "2019-03-21");
        System.out.println("若下面没出现“无缓存的时候调用”字样且能打印出数据表示测试成功");
        return user;
    }
}
>>>>>>> 4df1014710707ff5f6b96287d5a4bfbece328af5
