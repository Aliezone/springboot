package com.aftery.bean;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String userName;
    private String passWorld;
    private String email;
    private String nickName;
    private String regTime;

    public User() {
    }
}
