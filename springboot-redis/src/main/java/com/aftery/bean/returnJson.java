package com.aftery.bean;

import lombok.Data;

@Data
public class returnJson {
    private  final  Integer code=200;
    private  final  String message="成功";
    private  String sessionId;
    private  Integer id;
    private  String port;
}
