package com.aftery.service;

import javax.mail.MessagingException;

public interface MailService {

    void sendSimpleMail(String to,String subject,String content);
    void sendHtmlMail(String to,String subject,String content);
    void  sendAttachmentsMail(String to,String subject,String content,String filePath) throws MessagingException;
    void  sendInlineResourceMail(String to,String subject,String content,String rscPath,String rscId);
}
