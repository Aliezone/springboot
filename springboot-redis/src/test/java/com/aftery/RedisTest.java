package com.aftery;

import com.aftery.bean.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTest {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public  void Test(){
        stringRedisTemplate.opsForValue().set("aaaa","11111");
        Assert.assertEquals("11111",stringRedisTemplate.opsForValue().get("aaaa"));
        System.out.println(stringRedisTemplate.opsForValue().get("aaaa"));
    }

    @Test
    public void testObj() throws  Exception{
       User user=new User(1L,"aa","123456","123456@qq.com","aa","2019-03-21");
        ValueOperations operations = redisTemplate.opsForValue();
        operations.set("com.aftery",user);
        operations.set("com.aftery.c",user,1, TimeUnit.SECONDS);
        Thread.sleep(1000);
        Boolean aBoolean = redisTemplate.hasKey("com.aftery.c");
        if(aBoolean){
            System.out.println("aBoolean is true");
        }else {
            System.out.println("aBoolean is false");
        }

    }
}
