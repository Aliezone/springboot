package com.aftery;

import com.aftery.service.MailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.MessagingException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MailServiceTest {
    
    @Autowired
    private MailService mailService;

    @Test
    public  void  testSimpleMail(){
        mailService.sendSimpleMail("1320265074@qq.com","test simple Mail","hello this is Simple mail");
    }
    @Test
    public  void testHtmlMail(){
        String content="<html>\n" +
                "    <body>\n" +
                "        <h3>hello world!这是一封html邮件</h3>\n" +
                "    </body>\n" +
                "</html>";
        mailService.sendHtmlMail("1320265074@qq.com","html mail",content);
    }
    @Test
    public  void testsendAttachmentsMail(){
        try {
            mailService.sendAttachmentsMail("1320265074@qq.com","test","有附件，请查收！","C:\\Users\\admin\\Desktop\\aa.html");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @Test
    public  void testsendInlineResourceMail(){
        String rscId="006";
        String conteng="<html><body>这是有图片的邮件：<img src=\'cid:" + rscId + "\' ></body></html>";
        String imgPath="C:\\Users\\admin\\Downloads\\18030.jpg";
        mailService.sendInlineResourceMail("1320265074@qq.com","主题：这是有图片的邮件",conteng,imgPath,rscId);
    }

}
