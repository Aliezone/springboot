package com.aftery.springbootapi.domain;

import lombok.Data;

import java.util.List;

@Data
public class images {
    private  String code;
    private  String message;
    private List<image> result;
}
