package com.aftery.springbootapi.service.impl;

import com.aftery.springbootapi.domain.image;
import com.aftery.springbootapi.mapper.imageMapper;
import com.aftery.springbootapi.service.imageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class imageServiceImpl implements imageService{

    @Autowired
    private imageMapper mapper;
    @Override
    public void saveImage(image image) {
        mapper.saveImage(image);
    }
}
