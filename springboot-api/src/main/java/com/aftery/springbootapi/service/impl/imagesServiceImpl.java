package com.aftery.springbootapi.service.impl;

import com.aftery.springbootapi.domain.images;
import com.aftery.springbootapi.mapper.imagesMapper;
import com.aftery.springbootapi.service.imagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class imagesServiceImpl implements imagesService{

    @Autowired
    private imagesMapper mapper;


    @Override
    public void saveImages(images images) {
        mapper.saveImages(images);
    }
}
