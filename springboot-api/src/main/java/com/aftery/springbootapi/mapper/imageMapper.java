package com.aftery.springbootapi.mapper;

import com.aftery.springbootapi.domain.image;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface imageMapper {

    @Insert("insert into image values(#{id},#{time},#{img})")
    void  saveImage(image image);
}
