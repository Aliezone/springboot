package com.aftery.springbootapi.mapper;

import com.aftery.springbootapi.domain.images;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface imagesMapper {

    @Insert("insert into images values(#{code},#{mesage})")
    void  saveImages(images images);
}
