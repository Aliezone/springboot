package com.aftery.springbootapi.web;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import com.aftery.springbootapi.domain.image;
import com.aftery.springbootapi.domain.images;

import com.aftery.springbootapi.service.impl.imageServiceImpl;
import com.aftery.springbootapi.service.impl.imagesServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class imageController {

    @Autowired
    private imageServiceImpl imageService;
    @Autowired
    private imagesServiceImpl imagesService;

    @RequestMapping("/add/{number}")
    public  String save(@PathVariable("number") Integer number ){
        for (int i = 60; i <=number ; i++) {
            String url="https://api.apiopen.top/getImages?page="+i;
            String json = HttpUtil.get(url);
            JSONObject jsonObject=new JSONObject(json);

            images images = jsonObject.toBean(images.class);
            List<image> list = images.getResult();
            for (image image : list) {
                imageService.saveImage(image);
            }
            //imagesService.saveImages(images);
        }
        return "seccuse";
    }



}
