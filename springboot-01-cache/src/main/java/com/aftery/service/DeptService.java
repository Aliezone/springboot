package com.aftery.service;

import com.aftery.bean.Department;
import com.aftery.mapper.DepartmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.stereotype.Service;

@Service
public class DeptService {

    @Autowired
    private DepartmentMapper mapper;
    @Autowired
    private RedisCacheManager deptRedisCacheManager;

  /*  @Cacheable(value = "dept",cacheManager = "deptCacheManager")
    public Department getDeptById(Integer id){
        System.out.println("查询部门："+id);
        return  mapper.getDeptById(id);
    }*/

    // 使用缓存管理器得到缓存，进行api调用
    public Department getDeptById(Integer id){
        System.out.println("查询部门"+id);
        Department department = mapper.getDeptById(id);

        //获取某个缓存
        Cache dept = deptRedisCacheManager.getCache("dept");
        dept.put("dept:1",department);

        return department;
    }
}
