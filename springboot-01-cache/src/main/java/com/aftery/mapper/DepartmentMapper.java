package com.aftery.mapper;

import com.aftery.bean.Department;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface DepartmentMapper {

    @Select("Select * from department where id=#{id}")
    Department getDeptById(Integer id);
}
