package com.aftery.mapper;

import com.aftery.bean.Employee;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;


public interface EmployeeMapper {

    @Select("select * from employee where id=#{id}")
    Employee getEmpById(Integer id);

    @Update("update employee set lastName=#{lastName},email=#{email},gender=#{gender},d_id=#{dId} where id=#{id}")
    void updateEmp(Employee employee);

    @Delete("delete from employee where id=#{id}")
    void deleteEmpById(Integer id);

    @Insert("inster into employee(lastNmae,email,gender,dId),vakues(#{lastNmae},#{email},#{gender},#{dId})")
    void insterUser(Employee employee);

    @Select("select * from employee where lastName=#{LastName}")
    Employee getEcployeeLastName(String LastName);
}
