package com.aftery.controller;

import com.aftery.bean.Department;
import com.aftery.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DeptControllert {
    @Autowired
    private DeptService deptService;




    @GetMapping("/dept/{id}")
    public Department getDept(@PathVariable("id") Integer id){
        return  deptService.getDeptById(id);
    }

}
